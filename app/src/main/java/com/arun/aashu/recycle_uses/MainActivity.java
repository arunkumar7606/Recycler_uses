package com.arun.aashu.recycle_uses;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


     RecyclerView recyclerView;
    ArrayList<MyPoja>  arrayList=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView= (RecyclerView) findViewById(R.id.recycleView1);


//        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
//        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this,1));

        MyRecyclerAdapter adapter=new MyRecyclerAdapter(MainActivity.this,Vishu());

        recyclerView.setAdapter(adapter);





    }
    public ArrayList<MyPoja> Vishu(){

        MyPoja obj1=new MyPoja("Aashu",R.drawable.aashu);
        MyPoja obj2=new MyPoja("Abhi",R.drawable.abhi);
        MyPoja obj3=new MyPoja("Suraj",R.drawable.suraj);
        MyPoja obj4=new MyPoja("Mohit",R.drawable.mohit);
        MyPoja obj5=new MyPoja("Nisha",R.drawable.nisha);
        MyPoja obj6=new MyPoja("Dhruv",R.drawable.dhruv);
        arrayList.add(obj1);
        arrayList.add(obj2);
        arrayList.add(obj3);
        arrayList.add(obj4);
        arrayList.add(obj5);
        arrayList.add(obj6);

        return arrayList;
    }
}
