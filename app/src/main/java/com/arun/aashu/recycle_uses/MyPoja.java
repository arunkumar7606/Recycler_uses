package com.arun.aashu.recycle_uses;

/**
 * Created by aAsHu on 4/5/2018.
 */

public class MyPoja {

    String AuthorName;
    Integer AuthorImage;

    public MyPoja() {
    }

    public MyPoja(String authorName, Integer authorImage) {
        AuthorName = authorName;
        AuthorImage = authorImage;
    }

    public String getAuthorName() {
        return AuthorName;
    }

    public void setAuthorName(String authorName) {
        AuthorName = authorName;
    }

    public Integer getAuthorImage() {
        return AuthorImage;
    }

    public void setAuthorImage(Integer authorImage) {
        AuthorImage = authorImage;
    }
}
