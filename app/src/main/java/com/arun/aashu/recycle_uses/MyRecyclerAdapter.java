package com.arun.aashu.recycle_uses;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by aAsHu on 4/5/2018.
 */

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder>{

     ArrayList<MyPoja> ArrayList;
     Context context;
     LayoutInflater layoutInflater;
    View view;

    public MyRecyclerAdapter() {
    }

    public MyRecyclerAdapter(Context context,ArrayList<MyPoja> myList) {
        ArrayList = myList;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public MyRecyclerAdapter.MyViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        view= layoutInflater.inflate(R.layout.jadu,null,false);

       MyRecyclerAdapter.MyViewHolder myViewHolder=new MyRecyclerAdapter.MyViewHolder(view);


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyRecyclerAdapter.MyViewHolder holder, int position) {

        holder.imageView.setImageResource(ArrayList.get(position).AuthorImage);
        holder.textView.setText(ArrayList.get(position).AuthorName);

    }

    @Override
    public int getItemCount() {
        return ArrayList.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView  textView;

        public MyViewHolder(View itemView) {
            super(itemView);

          imageView=itemView.findViewById(R.id.imgView);
            textView=itemView.findViewById(R.id.txtView);



        }
    }
}
